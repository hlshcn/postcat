import { COMMON_CONFIG } from 'eo/workbench/browser/src/environments/common';
export const APP_CONFIG = {
  serverUrl: 'http://54.255.141.14:8080',
  production: false,
  environment: 'DEV',
  ...COMMON_CONFIG
};
